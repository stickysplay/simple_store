import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:simple_store/dto/product_dto.dart';
import 'package:simple_store/repository/products_repo.dart';

part 'products_event.dart';
part 'products_state.dart';
part 'products_bloc.freezed.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsRepo repo;

  ProductsBloc({
    required this.repo,
  }) : super(const Initial()) {
    on<FilterProducts>(_onFilterProducts);
  }

  _onFilterProducts(
    FilterProducts event,
    Emitter<ProductsState> emit,
  ) async {
    emit(const LoadInProgress());
    final result = await repo.fecthProducts(sort: event.sort);
    final filteredList = result.productsList!.where((item) {
      final category = event.category == 'category' ? null : event.category;
      if (category != null) {
        return item.category == category &&
            item.rating.rate.floor() >= double.parse(event.rating ?? '1');
      }
      return item.rating.rate.floor() >= double.parse(event.rating ?? '1');
    }).toList();
    if (result.errorMessage != null) {
      emit(LoadFailure(result.errorMessage!));
    } else {
      emit(LoadSuccess(productsList: filteredList));
    }
  }
}
