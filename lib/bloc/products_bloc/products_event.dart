part of 'products_bloc.dart';

@freezed
class ProductsEvent with _$ProductsEvent {
  const factory ProductsEvent.filterProducts({
    String? category,
    String? rating,
    String? sort,
  }) = FilterProducts;
}
