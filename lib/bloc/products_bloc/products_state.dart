part of 'products_bloc.dart';

@freezed
class ProductsState with _$ProductsState {
  const factory ProductsState.initial() = Initial;
  const factory ProductsState.loadInProgress() = LoadInProgress;
  const factory ProductsState.loadSuccess({required List<Product> productsList}) =
      LoadSuccess;
  const factory ProductsState.loadFailure(String errorText) = LoadFailure;
}
