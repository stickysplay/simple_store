part of 'product_details_bloc.dart';

@freezed
class ProductDetailsEvent with _$ProductDetailsEvent {
  const factory ProductDetailsEvent.started() = Started;
  const factory ProductDetailsEvent.getProductDetails({required int id}) =
      GetProductDetails;
}
