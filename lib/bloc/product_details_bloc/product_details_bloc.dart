import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:simple_store/dto/product_dto.dart';
import 'package:simple_store/repository/products_repo.dart';

part 'product_details_event.dart';
part 'product_details_state.dart';
part 'product_details_bloc.freezed.dart';

class ProductDetailsBloc
    extends Bloc<ProductDetailsEvent, ProductDetailsState> {
  final ProductsRepo repo;

  ProductDetailsBloc({
    required this.repo,
  }) : super(const Initial()) {
    on<GetProductDetails>(_onGetProductDetails);
  }

  _onGetProductDetails(
    GetProductDetails event,
    Emitter<ProductDetailsState> emit,
  ) async {
    emit(const LoadInProgress());
    final result = await repo.getProductDetails(event.id);
    if (result.errorMessage != null) {
      emit(
        LoadFailure(result.errorMessage!),
      );
    } else {
      emit(
        LoadSuccess(product: result.productDetails!),
      );
    }
  }
}
