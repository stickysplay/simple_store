part of 'product_details_bloc.dart';

@freezed
class ProductDetailsState with _$ProductDetailsState {
  const factory ProductDetailsState.initial() = Initial;
  const factory ProductDetailsState.loadInProgress() = LoadInProgress;
  const factory ProductDetailsState.loadSuccess({required Product product}) =
      LoadSuccess;
  const factory ProductDetailsState.loadFailure(String errorText) = LoadFailure;
}
