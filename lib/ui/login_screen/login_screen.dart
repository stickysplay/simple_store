import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/repository/settings_repo.dart';
import 'package:simple_store/ui/login_screen/widgets/custom_text_form_field.dart';
import 'package:simple_store/ui/login_screen/widgets/login_alert_dialog.dart';
import 'package:simple_store/ui/store_screen/store_screen.dart';
import 'package:simple_store/utils/app_assets.dart';
import 'package:simple_store/utils/helpers/app_unfocuser.dart';
import 'package:simple_store/utils/style/style.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static const String routeName = '/login';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _loginController;
  late TextEditingController _passwordController;
  late FocusNode _passwordFocusNode;

  @override
  void initState() {
    super.initState();
    _loginController = TextEditingController();
    _passwordController = TextEditingController();
    _passwordFocusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppUnfocuser(
        child: Padding(
          padding: const EdgeInsets.all(26),
          child: Column(
            children: [
              Expanded(
                child: SvgPicture.asset(AppAssets.svg.simpleStoreLogo),
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(S.of(context).login),
                    const SizedBox(height: 8),
                    CustomTextFormField(
                      controller: _loginController,
                      label: S.of(context).login,
                      validator: _loginValidator,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_passwordFocusNode);
                      },
                      prefix: AppAssets.svg.account,
                    ),
                    const SizedBox(height: 10),
                    Text(S.of(context).password),
                    const SizedBox(height: 8),
                    CustomTextFormField(
                      controller: _passwordController,
                      label: S.of(context).password,
                      validator: _passwordValidator,
                      focusNode: _passwordFocusNode,
                      obscureText: true,
                      prefix: AppAssets.svg.password,
                      suffix: Icons.visibility,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 24),
                child: ElevatedButton(
                  onPressed: _signIn,
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(double.maxFinite, 48),
                  ),
                  child: Text(S.of(context).signIn),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    S.of(context).dontHaveAnAccount,
                    style: AppStyles.s14w400.copyWith(
                      color: AppColors.neutral3,
                    ),
                  ),
                  const SizedBox(width: 10),
                  InkWell(
                    onTap: () {},
                    child: Text(
                      S.of(context).create,
                      style: AppStyles.s14w400.copyWith(
                        color: AppColors.primary,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _loginController.dispose();
    _passwordController.dispose();
    _passwordFocusNode.dispose();
  }

  String? _loginValidator(String? login) {
    if (login == null || login.isEmpty) {
      return S.of(context).inputErrorCheckLogin;
    }
    if (login.length < 3) {
      return S.of(context).inputErrorLoginIsShort;
    }
    return null;
  }

  String? _passwordValidator(String? password) {
    if (password == null || password.isEmpty) {
      return S.of(context).inputErrorCheckPassword;
    }
    if (password.length < 8) {
      return S.of(context).inputErrorPasswordIsShort;
    }
    return null;
  }

  void _signIn() {
    final isValidated = _formKey.currentState?.validate() ?? false;
    if (isValidated) {
      FocusScope.of(context).unfocus();
      _formKey.currentState?.save();
      if (_loginController.text == 'qwerty' &&
          _passwordController.text == '123456ab') {
        Navigator.of(context).pushNamedAndRemoveUntil(
          StoreScreen.routeName,
          (route) => false,
        );
        context.read<SettingsRepo>().authorize();
      } else {
        showDialog(
          context: context,
          builder: (context) => const LoginAlertDialog(),
        );
        _loginController.clear();
        _passwordController.clear();
      }
    }
  }
}
