import 'package:flutter/material.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/utils/style/style.dart';

class LoginAlertDialog extends StatelessWidget {
  const LoginAlertDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        S.of(context).error,
        style: AppStyles.s20w500,
      ),
      content: Text(S.of(context).incorrectLoginOrPassword),
      insetPadding: const EdgeInsets.all(28),
      titlePadding: const EdgeInsets.only(
        left: 30,
        top: 30,
        right: 30,
      ),
      contentPadding: const EdgeInsets.only(
        left: 30,
        top: 24,
        right: 30,
      ),
      buttonPadding: const EdgeInsets.only(left: 30, right: 30, top: 0),
      actions: [
        OutlinedButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          style: OutlinedButton.styleFrom(
            minimumSize: const Size(double.maxFinite, 40),
          ),
          child: const Text('Ok'),
        ),
      ],
    );
  }
}
