import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_store/bloc/product_details_bloc/product_details_bloc.dart';
import 'package:simple_store/bloc/products_bloc/products_bloc.dart';
import 'package:simple_store/dto/product_dto.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/ui/product_details_screen/product_details_screen.dart';
import 'package:simple_store/ui/store_screen/widgets/category_dropdown_button.dart';
import 'package:simple_store/ui/store_screen/widgets/product_card.dart';
import 'package:simple_store/ui/store_screen/widgets/rating_dropdown_button.dart';
import 'package:simple_store/ui/store_screen/widgets/sort_button.dart';
import 'package:simple_store/utils/style/app_colors.dart';
import 'package:simple_store/widgets/app_bottom_nav_bar.dart';

class StoreScreen extends StatefulWidget {
  const StoreScreen({Key? key}) : super(key: key);
  static const routeName = '/store_screen';

  @override
  State<StoreScreen> createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {
  String _categoryDropdownValue = 'category';
  String _ratingDropdownValue = '1';
  bool _sortAsc = true;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const AppBottomNavBar(currentIndex: 0),
        appBar: AppBar(
          title: Text(S.of(context).products),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(36),
            child: ColoredBox(
              color: AppColors.neutral2,
              child: Row(
                children: [
                  CategoryDropdownButton(
                    dropdownValue: _categoryDropdownValue,
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          _categoryDropdownValue = value;
                          BlocProvider.of<ProductsBloc>(context).add(
                            FilterProducts(
                              category: _categoryDropdownValue,
                              rating: _ratingDropdownValue,
                            ),
                          );
                        });
                      }
                    },
                  ),
                  RatingDropdownButton(
                    dropdownValue: _ratingDropdownValue,
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          _ratingDropdownValue = value;
                          BlocProvider.of<ProductsBloc>(context).add(
                            FilterProducts(
                              category: _categoryDropdownValue,
                              rating: _ratingDropdownValue,
                            ),
                          );
                        });
                      }
                    },
                  ),
                  const Spacer(),
                  SortButton(
                    onTap: () {
                      setState(() {
                        _sortAsc = !_sortAsc;
                      });
                      BlocProvider.of<ProductsBloc>(context).add(
                        FilterProducts(
                          sort: _sortAsc ? 'asc' : 'desc',
                          category: _categoryDropdownValue,
                          rating: _ratingDropdownValue,
                        ),
                      );
                    },
                    sortAsc: _sortAsc,
                  ),
                ],
              ),
            ),
          ),
        ),
        body: BlocBuilder<ProductsBloc, ProductsState>(
          builder: (context, state) => state.map(
            initial: (state) => Container(
              color: Colors.green,
            ),
            loadInProgress: (_) =>
                const Center(child: CircularProgressIndicator()),
            loadFailure: (state) => Center(
              child: Text(S.of(context).somethingWentWrong),
            ),
            loadSuccess: (state) => _StoreScreenBody(
              productsList: state.productsList,
            ),
          ),
        ),
      ),
    );
  }
}

class _StoreScreenBody extends StatelessWidget {
  const _StoreScreenBody({
    Key? key,
    required this.productsList,
  }) : super(key: key);

  final List<Product> productsList;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: productsList.length,
      itemBuilder: (context, index) {
        final product = productsList[index];
        return ProductCard(
          image: product.image,
          title: product.title,
          price: product.price,
          rating: product.rating.rate,
          id: product.id,
          onTap: () {
            BlocProvider.of<ProductDetailsBloc>(context).add(
              ProductDetailsEvent.getProductDetails(id: product.id),
            );
            Navigator.of(context).pushNamed(ProductDetailsScreen.routeName);
          },
        );
      },
    );
  }
}
