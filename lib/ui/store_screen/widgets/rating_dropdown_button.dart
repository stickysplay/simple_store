import 'package:flutter/material.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/utils/style/style.dart';

class RatingDropdownButton extends StatelessWidget {
  const RatingDropdownButton({
    Key? key,
    required this.dropdownValue,
    required this.onChanged,
  }) : super(key: key);

  final String dropdownValue;
  final Function(String?)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      margin: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: AppColors.primary,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(color: Colors.black54),
      ),
      child: DropdownButton<String>(
        value: dropdownValue,
        isDense: true,
        underline: const SizedBox(),
        items: [
          DropdownMenuItem(
            value: '1',
            child: Text(S.of(context).all),
          ),
          const DropdownMenuItem(
            value: '2',
            child: Text('2 ☆'),
          ),
          const DropdownMenuItem(
            value: '3',
            child: Text('3 ☆'),
          ),
          const DropdownMenuItem(
            value: '4',
            child: Text('4 ☆'),
          ),
          const DropdownMenuItem(
            value: '5',
            child: Text('5 ☆'),
          ),
        ],
        onChanged: onChanged,
      ),
    );
  }
}
