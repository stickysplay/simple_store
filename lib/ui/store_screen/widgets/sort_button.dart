import 'package:flutter/material.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/utils/style/style.dart';

class SortButton extends StatelessWidget {
  const SortButton({
    Key? key,
    required this.onTap,
    required this.sortAsc,
  }) : super(key: key);

  final VoidCallback onTap;
  final bool sortAsc;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 3,
        ),
        margin: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: AppColors.primary,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: Colors.black54),
        ),
        child: Row(
          children: [
            Text(
              S.of(context).sortByID,
              style: AppStyles.s14w500.copyWith(color: AppColors.mainText),
            ),
            Icon(
              sortAsc
                  ? Icons.keyboard_double_arrow_up
                  : Icons.keyboard_double_arrow_down,
              size: 18,
              color: AppColors.mainText,
            ),
          ],
        ),
      ),
    );
  }
}
