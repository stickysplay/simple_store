import 'package:flutter/material.dart';
import 'package:simple_store/utils/style/style.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({
    Key? key,
    required this.image,
    required this.title,
    required this.price,
    required this.rating,
    required this.id,
    required this.onTap,
  }) : super(key: key);

  final String image;
  final String title;
  final double price;
  final double rating;
  final int id;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: onTap,
        child: Card(
          child: Row(
            children: [
              Container(
                height: 120,
                width: 120,
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                  ),
                ),
                child: Image.network(
                  image,
                  fit: BoxFit.contain,
                ),
              ),
              Expanded(
                child: Container(
                  height: 120,
                  padding: const EdgeInsets.all(8),
                  color: AppColors.primary,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: AppStyles.s18w500,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        'Price: \$$price',
                        style: AppStyles.s16w400,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Rating: $rating',
                            style: AppStyles.s16w400,
                          ),
                          Text(
                            'ID: $id',
                            style: AppStyles.s16w500,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const Icon(Icons.keyboard_arrow_right),
            ],
          ),
        ),
      ),
    );
  }
}
