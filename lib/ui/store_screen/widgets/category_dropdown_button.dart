import 'package:flutter/material.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/utils/style/app_colors.dart';

class CategoryDropdownButton extends StatelessWidget {
  const CategoryDropdownButton({
    Key? key,
    required this.dropdownValue,
    required this.onChanged,
  }) : super(key: key);

  final String dropdownValue;
  final Function(String?)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      margin: const EdgeInsets.all(8),
      width: 140,
      decoration: BoxDecoration(
        color: AppColors.primary,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(color: Colors.black54),
      ),
      child: DropdownButton<String>(
        value: dropdownValue,
        isDense: true,
        isExpanded: true,
        underline: const SizedBox(),
        items: [
          DropdownMenuItem(
            value: 'category',
            child: Text(
              S.of(context).category,
              overflow: TextOverflow.fade,
              softWrap: false,
            ),
          ),
          DropdownMenuItem(
            value: 'electronics',
            child: Text(
              S.of(context).electronics,
              overflow: TextOverflow.fade,
              softWrap: false,
            ),
          ),
          DropdownMenuItem(
            value: 'jewelery',
            child: Text(
              S.of(context).jewelery,
              overflow: TextOverflow.fade,
              softWrap: false,
            ),
          ),
          DropdownMenuItem(
            value: 'men\'s clothing',
            child: Text(
              S.of(context).mensClothing,
              overflow: TextOverflow.fade,
              softWrap: false,
            ),
          ),
          DropdownMenuItem(
            value: 'women\'s clothing',
            child: Text(
              S.of(context).womensClothing,
              overflow: TextOverflow.fade,
              softWrap: false,
            ),
          ),
        ],
        onChanged: onChanged,
      ),
    );
  }
}
