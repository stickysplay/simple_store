import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_store/bloc/product_details_bloc/product_details_bloc.dart';
import 'package:simple_store/dto/product_dto.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/utils/style/style.dart';

class ProductDetailsScreen extends StatelessWidget {
  static const routeName = '/productDetails';
  const ProductDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).productDetails),
      ),
      body: Padding(
        padding: const EdgeInsets.all(26),
        child: BlocBuilder<ProductDetailsBloc, ProductDetailsState>(
          builder: (context, state) => state.map(
            initial: (_) => const Center(
              child: CircularProgressIndicator(),
            ),
            loadInProgress: (_) => const Center(
              child: CircularProgressIndicator(),
            ),
            loadSuccess: (state) =>
                _ProductDetailsScreen(product: state.product),
            loadFailure: (state) => Center(
              child: Text(S.of(context).somethingWentWrong),
            ),
          ),
        ),
      ),
    );
  }
}

class _ProductDetailsScreen extends StatelessWidget {
  const _ProductDetailsScreen({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            product.title,
            style: AppStyles.s14w500,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 24),
            child: Image.network(
              product.image,
              height: MediaQuery.of(context).size.height * 0.5,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 6,
                ),
                decoration: BoxDecoration(
                  color: AppColors.neutral1,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Text(
                  '${product.rating.rate}',
                  style: AppStyles.s16w500,
                ),
              ),
              Text(
                '\$${product.price}',
                style: AppStyles.s16w700,
              ),
            ],
          ),
          const SizedBox(height: 34),
          Text(
            product.description,
            style: AppStyles.s14w500,
          ),
        ],
      ),
    );
  }
}
