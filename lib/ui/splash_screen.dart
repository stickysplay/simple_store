import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/repository/settings_repo.dart';
import 'package:simple_store/ui/login_screen/login_screen.dart';
import 'package:simple_store/ui/store_screen/store_screen.dart';
import 'package:simple_store/utils/app_assets.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    final settingsRepo = context.read<SettingsRepo>();
// Искусственная задержка, чтобы засветить этот шикарный Сплеш скрин с дизайном на миллион
    Future.delayed(const Duration(seconds: 2)).whenComplete(() {
      return settingsRepo.init().whenComplete(
        () async {
          var defaultLocale = const Locale('ru', 'RU');
          final locale = await settingsRepo.readLocale();
          final isAuthorized = await settingsRepo.isAuthorized() ?? false;
          if (locale == 'en') {
            defaultLocale = const Locale('en');
          }
          S.load(defaultLocale).whenComplete(() {
            Navigator.of(context).pushNamedAndRemoveUntil(
              isAuthorized ? StoreScreen.routeName : LoginScreen.routeName,
              (_) => false,
            );
          });
        },
      );
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Image.asset(
            AppAssets.images.splashBackground,
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          bottom: 36,
          left: 55,
          right: 55,
          child: SvgPicture.asset(
            AppAssets.svg.simpleStoreLogo,
            alignment: Alignment.bottomCenter,
          ),
        ),
      ],
    );
  }
}
