import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/repository/settings_repo.dart';
import 'package:simple_store/ui/login_screen/login_screen.dart';
import 'package:simple_store/ui/settings_screen/widgets/language_dropdown_button.dart';
import 'package:simple_store/utils/style/app_colors.dart';
import 'package:simple_store/widgets/app_bottom_nav_bar.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);
  static const String routeName = '/settings';

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const AppBottomNavBar(currentIndex: 1),
      appBar: AppBar(
        title: Text(S.of(context).settings),
        actions: [
          InkWell(
            onTap: () {
              context.read<SettingsRepo>().logout().whenComplete(
                    () => Navigator.pushNamedAndRemoveUntil(
                      context,
                      LoginScreen.routeName,
                      (_) => false,
                    ),
                  );
            },
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(
                Icons.logout,
                color: AppColors.primary,
              ),
            ),
          ),
        ],
      ),
      body: Center(
        child: LanguageDropDownButton(onChanged: _onChanged),
      ),
    );
  }

  void _onChanged(String? value) async {
    if (value == null) return;
    if (value == 'ru_RU') {
      await S.load(const Locale('ru', 'RU'));
      setState(() {});
    } else if (value == 'en') {
      await S.load(const Locale('en'));
      setState(() {});
    }
    if (!mounted) return;
    final settingsRepo = context.read<SettingsRepo>();
    settingsRepo.saveLocal(value);
  }
}
