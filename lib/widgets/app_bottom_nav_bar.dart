import 'package:flutter/material.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/ui/settings_screen/settings_screen.dart';
import 'package:simple_store/ui/store_screen/store_screen.dart';
import 'package:simple_store/utils/helpers/no_animation_page_route_builder.dart';

class AppBottomNavBar extends StatelessWidget {
  const AppBottomNavBar({
    super.key,
    required this.currentIndex,
  });

  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      items: [
        BottomNavigationBarItem(
          icon: const Icon(Icons.local_grocery_store),
          label: S.of(context).products,
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.settings),
          label: S.of(context).settings,
        ),
      ],
      onTap: (index) {
        if (index == 0 && currentIndex != 0) {
          Navigator.of(context).pushAndRemoveUntil(
            NoAnimationPageRouteBuilder(route: const StoreScreen()),
            (_) => false,
          );
        } else if (index == 1 && currentIndex != 1) {
          Navigator.of(context).pushAndRemoveUntil(
            NoAnimationPageRouteBuilder(route: const SettingsScreen()),
            (_) => false,
          );
        }
      },
    );
  }
}
