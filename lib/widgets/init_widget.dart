import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_store/bloc/product_details_bloc/product_details_bloc.dart';
import 'package:simple_store/bloc/products_bloc/products_bloc.dart';
import 'package:simple_store/repository/api.dart';
import 'package:simple_store/repository/products_repo.dart';
import 'package:simple_store/repository/settings_repo.dart';

class InitWidget extends StatelessWidget {
  const InitWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => Api(),
        ),
        RepositoryProvider(
          create: (context) => SettingsRepo(),
        ),
        RepositoryProvider(
          create: (context) => ProductsRepo(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<ProductsBloc>(
            create: (context) => ProductsBloc(
              repo: RepositoryProvider.of<ProductsRepo>(context),
            )..add(const FilterProducts()),
          ),
          BlocProvider<ProductDetailsBloc>(
            create: (context) => ProductDetailsBloc(
              repo: RepositoryProvider.of<ProductsRepo>(context),
            ),
          ),
        ],
        child: child,
      ),
    );
  }
}
