import 'package:flutter/material.dart';
import 'package:simple_store/utils/style/style.dart';

ThemeData appTheme = ThemeData(
  //Здесь можно useMaterial3: true поставить. но тогда цвета приложения начнут меняться динамически
  // ignore: deprecated_member_use
  androidOverscrollIndicator: AndroidOverscrollIndicator.stretch,
  primaryColor: AppColors.primary,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      textStyle: AppStyles.s16w400,
      primary: AppColors.primary,
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      primary: AppColors.primary,
      side: const BorderSide(color: AppColors.primary),
      textStyle: AppStyles.s16w400,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
    ),
  ),
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    selectedLabelStyle: AppStyles.s12w400.copyWith(color: AppColors.primary),
    selectedItemColor: AppColors.primary,
    unselectedLabelStyle: AppStyles.s12w400.copyWith(color: AppColors.neutral1),
    unselectedItemColor: AppColors.neutral1,
    backgroundColor: AppColors.neutral3,
  ),
  appBarTheme: AppBarTheme(
    backgroundColor: AppColors.neutral3,
    centerTitle: true,
    elevation: 0,
    titleTextStyle: AppStyles.s20w500.copyWith(color: AppColors.primary),
  ),
  scaffoldBackgroundColor: AppColors.background,
  cardTheme: CardTheme(
    elevation: 4,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
      side: const BorderSide(color: Colors.black54),
    ),
  ),
);
