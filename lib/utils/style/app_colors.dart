import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xFFFFC107);
  static const background = Color(0xFFFFFFFF);
  static const mainText = Color(0xFF0B1E2D);
  static const neutral1 = Color(0xFFD9D9D9);
  static const neutral2 = Color(0xFF393939);
  static const neutral3 = Color(0xFF282828);
  static const kLightGreen = Color(0xFF43D049);
  static const kRed = Color(0xFFEB5757);
}
