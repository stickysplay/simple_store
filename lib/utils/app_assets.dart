class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();

  final String splashBackground = 'assets/images/bitmap/background.png';
}

class _Svg {
  const _Svg();

  final String simpleStoreLogo = 'assets/images/svg/simple_store_logo.svg';
  final String account = 'assets/images/svg/account.svg';
  final String password = 'assets/images/svg/password.svg';
}
