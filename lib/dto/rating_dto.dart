import 'package:freezed_annotation/freezed_annotation.dart';

part 'rating_dto.g.dart';
part 'rating_dto.freezed.dart';

@freezed
class Rating with _$Rating {
  const factory Rating({
    required double rate,
    required int count,
  }) = _Rating;

  factory Rating.fromJson(Map<String, dynamic> json) => _$RatingFromJson(json);
}
