import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:simple_store/dto/rating_dto.dart';

part 'product_dto.freezed.dart';
part 'product_dto.g.dart';

@freezed
class Product with _$Product {
  const factory Product({
    required int id,
    required String title,
    required double price,
    required String description,
    required String category,
    required String image,
    required Rating rating,
  }) = _Product;

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}
