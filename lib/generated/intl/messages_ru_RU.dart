// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_RU locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_RU';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "all": MessageLookupByLibrary.simpleMessage("Все"),
        "auth": MessageLookupByLibrary.simpleMessage("Авторизация"),
        "category": MessageLookupByLibrary.simpleMessage("Категория"),
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "create": MessageLookupByLibrary.simpleMessage("Создать"),
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("У вас еще нет аккаунта?"),
        "electronics": MessageLookupByLibrary.simpleMessage("Электроника"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enterLoginAndPassword":
            MessageLookupByLibrary.simpleMessage("Введите логин и пароль"),
        "error": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "incorrectLoginOrPassword": MessageLookupByLibrary.simpleMessage(
            "Введен неверный логин или пароль"),
        "inputErrorCheckLogin":
            MessageLookupByLibrary.simpleMessage("Проверьте логин"),
        "inputErrorCheckPassword":
            MessageLookupByLibrary.simpleMessage("Проверьте пароль"),
        "inputErrorLoginIsShort": MessageLookupByLibrary.simpleMessage(
            "Логин должен содержать не менее 3 символов"),
        "inputErrorPasswordIsShort": MessageLookupByLibrary.simpleMessage(
            "Пароль должен содержать не менее 8 символов"),
        "jewelery": MessageLookupByLibrary.simpleMessage("Украшения"),
        "language": MessageLookupByLibrary.simpleMessage("Язык"),
        "login": MessageLookupByLibrary.simpleMessage("Логин"),
        "mensClothing": MessageLookupByLibrary.simpleMessage("Мужская одежда"),
        "noData": MessageLookupByLibrary.simpleMessage("Нет данных"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "product": MessageLookupByLibrary.simpleMessage("Продукт"),
        "productDetails":
            MessageLookupByLibrary.simpleMessage("Описание Продукта"),
        "products": MessageLookupByLibrary.simpleMessage("Продукты"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "signIn": MessageLookupByLibrary.simpleMessage("Войти"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Что-то пошло не так!"),
        "sortByID": MessageLookupByLibrary.simpleMessage("Сорт. по ID: "),
        "womensClothing": MessageLookupByLibrary.simpleMessage("Женская одежда")
      };
}
