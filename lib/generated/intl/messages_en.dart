// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "all": MessageLookupByLibrary.simpleMessage("All"),
        "auth": MessageLookupByLibrary.simpleMessage("Authorization"),
        "category": MessageLookupByLibrary.simpleMessage("Category"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "create": MessageLookupByLibrary.simpleMessage("Create"),
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Do not have an account yet?"),
        "electronics": MessageLookupByLibrary.simpleMessage("Electronics"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enterLoginAndPassword":
            MessageLookupByLibrary.simpleMessage("Enter login and password"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "incorrectLoginOrPassword":
            MessageLookupByLibrary.simpleMessage("Incorrect login or password"),
        "inputErrorCheckLogin":
            MessageLookupByLibrary.simpleMessage("Check login"),
        "inputErrorCheckPassword":
            MessageLookupByLibrary.simpleMessage("Check password"),
        "inputErrorLoginIsShort": MessageLookupByLibrary.simpleMessage(
            "Login must contain more than 3 symbols"),
        "inputErrorPasswordIsShort": MessageLookupByLibrary.simpleMessage(
            "Password must contain more than 8 symbols"),
        "jewelery": MessageLookupByLibrary.simpleMessage("Jewelery"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "mensClothing": MessageLookupByLibrary.simpleMessage("Men\'s clothing"),
        "noData": MessageLookupByLibrary.simpleMessage("No data"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "product": MessageLookupByLibrary.simpleMessage("Product"),
        "productDetails":
            MessageLookupByLibrary.simpleMessage("Product Details"),
        "products": MessageLookupByLibrary.simpleMessage("Products"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "signIn": MessageLookupByLibrary.simpleMessage("Sign in"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Somehing went wrong!"),
        "sortByID": MessageLookupByLibrary.simpleMessage("Sort by ID: "),
        "womensClothing":
            MessageLookupByLibrary.simpleMessage("Women\'s clothing")
      };
}
