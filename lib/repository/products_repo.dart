import 'dart:developer';

import 'package:simple_store/dto/product_dto.dart';
import 'package:simple_store/repository/api.dart';

class ProductsRepo {
  ProductsRepo({required this.api});
  final Api api;

  Future<ResultProductsRepo> fecthProducts({String? sort}) async {
    try {
      final result = await api.dio.get('?sort=$sort');
      final List productListJson = result.data;
      final productsList = productListJson
          .map(
            (product) => Product.fromJson(product),
          )
          .toList();
      return ResultProductsRepo(productsList: productsList);
    } catch (e) {
      log('PRODUCTS_FETCH_ERROR: $e');
      return ResultProductsRepo(errorMessage: e.toString());
    }
  }

  Future<ResultProductsRepo> getProductDetails(int id) async {
    try {
      final result = await api.dio.get('/$id');
      final productJson = result.data;
      final productDetails = Product.fromJson(productJson);
      return ResultProductsRepo(productDetails: productDetails);
    } catch (e) {
      log('PRODUCTS_FETCH_ERROR: $e');
      return ResultProductsRepo(errorMessage: e.toString());
    }
  }
}

class ResultProductsRepo {
  ResultProductsRepo({
    this.errorMessage,
    this.productsList,
    this.productDetails,
  });

  final String? errorMessage;
  final List<Product>? productsList;
  final Product? productDetails;
}
