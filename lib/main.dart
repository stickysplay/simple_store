import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:simple_store/generated/l10n.dart';
import 'package:simple_store/ui/login_screen/login_screen.dart';
import 'package:simple_store/ui/product_details_screen/product_details_screen.dart';
import 'package:simple_store/ui/settings_screen/settings_screen.dart';
import 'package:simple_store/ui/splash_screen.dart';
import 'package:simple_store/ui/store_screen/store_screen.dart';
import 'package:simple_store/utils/style/app_theme.dart';
import 'package:simple_store/widgets/init_widget.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const SimpleStoreApp());
}

class SimpleStoreApp extends StatelessWidget {
  const SimpleStoreApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InitWidget(
      child: MaterialApp(
        title: 'SimpleStore',
        debugShowCheckedModeBanner: false,
        home: const SplashScreen(),
        theme: appTheme,
        routes: {
          StoreScreen.routeName: (context) => const StoreScreen(),
          LoginScreen.routeName: (context) => const LoginScreen(),
          ProductDetailsScreen.routeName: (context) =>
              const ProductDetailsScreen(),
          SettingsScreen.routeName: (context) => const SettingsScreen(),
        },
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
      ),
    );
  }
}
